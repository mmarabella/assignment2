from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
         {'title': 'Algorithm Design', 'id':'2'}, \
         {'title': 'Python', 'id':'3'}]
next_id = 4

@app.route('/book/JSON')
def bookJSON():
    json_books = jsonify(books)
    return(json_books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showPage.html', books = books)
    
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        book_title = request.form['title']
        global next_id
        book_id = str(next_id)
        next_id += 1
        books.append({'title': book_title, 'id': book_id})
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    book = {}
    for item in books:
        if item['id'] == str(book_id):
            book = item

    if request.method == 'POST':
        book['title'] = request.form['title']
        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', book=book)
    
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    book = {}
    for item in books:
        if item['id'] == str(book_id):
            book = item

    if request.method == 'POST':
        index = books.index(book)
        books.pop(index)
        return redirect(url_for('showBook'))
    else:
        return render_template('deleteBook.html', book=book)

if __name__ == '__main__':
    app.debug = True
    app.run(host = '0.0.0.0', port = 5000)
    

